<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers;
// use App\Http\Controllers\

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('website.index');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// Route::resource ('produk', ProdukController::class);

// Route::get('/vproduk', function () {
//     return view('produk.vproduk');
// });

// Route::get('/vproduk', function () {
//     return view('produk.index');
// });

Route::resource('produk', ProdukController::class);
Route::get('/vproduk', function () {
    return view('kategori.vkategori');
});

Route::resource ('kategori', KategoriController::class);

Route::get('/vkategori', function () {
    return view('kategori.vkategori');
});

Route::get('/addkategori', function () {
    return view('kategori.addkategori');
});

Route::resource ('customer', CustomerController::class);
Route::get('/vcustomer', function () {
    return view('customer.vcustomer');
});
Route::get('/addcustomer', function () {
    return view('customer.addcustomer');
});


