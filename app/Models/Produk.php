<?php

namespace App\Models;
// namespace produk;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
    use HasFactory;
    protected $table = "produk";
    // protected $primaryKey = "id";
    protected $fillable = ['kode_produk', 'nama_produk', 'img', 'desk', 'harga', 'stok'];
}
