<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Models\Produk;


class ProdukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $produksa = Produk::all();
        // return view('produk.index', compact('produksa'));
        // return view('produk.index', ['produksa' => $produksa]);

        // $data = Produk::latest()->paginate(5);
        $produk = Produk::all();

        return view('produk.index', compact(
            'produk'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('produk.addproduk');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'kode_produk' => 'required',
            'nama_produk' => 'required',
            'img' => 'required',
            'desk' => 'required',
            'harga' => 'required',
            'stok' => 'required',
        ]);

        Project::create($request->all());

        return redirect()->route('produk.index')
            ->with('success', 'Project created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Produk $produk)
    {
        return view('produk.show',compact('produk'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $produk = Produk::findOrFail($id);

        return view('produk.editproduk', compact(
            'produk'
        ));
        // return view('produk.editproduk',compact('produk'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'kode_produk' => 'required',
            'nama_produk' => 'required',
            'img' => 'required',
            'desk' => 'required',
            'harga' => 'required',
            'stok' => 'required',
        ]);

        Project::create($request->all());

        return redirect()->route('produk.index')
            ->with('success', 'Produk created successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $produk->delete();

        return redirect()->route('produk.index')
            ->with('success', 'Produk delete successfully.');
    }
}
