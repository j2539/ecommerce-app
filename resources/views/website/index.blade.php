<!DOCTYPE html>
<html lang="en">
   <head>
      <!-- basic -->
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- mobile metas -->
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="viewport" content="initial-scale=1, maximum-scale=1">
      <!-- site metas -->
      <title>Keril</title>
      <meta name="keywords" content="">
      <meta name="description" content="">
      <meta name="author" content="">
      <!-- bootstrap css -->
      <link rel="stylesheet" type="text/css" href="{!! asset('assets_website/css/bootstrap.min.css') !!}">
      <!-- style css -->
      <link rel="stylesheet" type="text/css" href="{!! asset('assets_website/css/style.css') !!}">
      <!-- Responsive-->
      <link rel="stylesheet" href="{!! asset('assets_website/css/responsive.css') !!}">
      <!-- fevicon -->
      <link rel="icon" href="{!! asset('assets_website/images/fevicon.png') !!}" type="image/gif" />
      <!-- Scrollbar Custom CSS -->
      <link rel="stylesheet" href="{!! asset('assets_website/css/jquery.mCustomScrollbar.min.css') !!}">
      <!-- Tweaks for older IEs-->
      <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
      <!-- fonts -->
      <link href="https://fonts.googleapis.com/css?family=Poppins:400,700&display=swap" rel="stylesheet">
      <!-- font awesome -->
      <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
      <!--  -->
      <!-- owl stylesheets -->
      <link href="https://fonts.googleapis.com/css?family=Great+Vibes|Poppins:400,700&display=swap&subset=latin-ext" rel="stylesheet">
      <link rel="stylesheet" href="{!! asset('assets_website/css/owl.carousel.min.css') !!}">
      <link rel="stylesoeet" href="{!! asset('assets_website/css/owl.theme.default.min.css') !!}">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" media="screen">
   </head>
   <body>
      <!-- banner bg main start -->
      @include('template-website.header')
      <!-- fashion section start -->
      <div class="fashion_section">
         <div id="main_slider" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
               <div class="carousel-item active">
                  <div class="container">
                     <h1 class="fashion_taital">Man & Woman Carrier Bags</h1>
                     <div class="fashion_section_2">
                        <div class="row">
                           <div class="col-lg-4 col-sm-4">
                              <div class="box_main">
                                 <h4 class="shirt_text">Consina Extraterrestrial</h4>
                                 <p class="price_text">Harga  <span style="color: #262626;">Rp. 5.699.000</span></p>
                                 <div class="tshirt_img"><img src="http://shop.consina-adventure.com/image/cache/data/product/PACKS/40%20LT%20TO%2060%20LT/alpine/2020/02112020-alpine55-BKGY-120x120.jpg"></div>
                                 <div class="btn_main">
                                    <div class="buy_bt"><a href="#">Buy Now</a></div>
                                    {{-- <div class="buy_bt"><a href="{{url('buy-produk')}}">Buy Now</a></div> --}}
                                    <div class="seemore_bt"><a href="#">See More</a></div>
                                    {{-- <div class="seemore_bt"><a href="{{ url('detail-produk') }}">See More</a></div> --}}
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-4 col-sm-4">
                              <div class="box_main">
                                 <h4 class="shirt_text">The North Face Terra</h4>
                                 <p class="price_text">Harga  <span style="color: #262626;">7.000.000</span></p>
                                 <div class="tshirt_img"><img src="https://images.thenorthface.com/is/image/TheNorthFace/NF0A3GA5_49C_hero?wid=875&hei=1017&fmt=jpeg&qlt=50&resMode=sharp2&op_usm=0.9,1.0,8,0"></div>
                                 <div class="btn_main">
                                    <div class="buy_bt"><a href="#">Buy Now</a></div>
                                    {{-- <div class="buy_bt"><a href="{{url('buy-produk')}}">Buy Now</a></div> --}}
                                    <div class="seemore_bt"><a href="#">See More</a></div>
                                    {{-- <div class="seemore_bt"><a href="{{ url('detail-produk') }}">See More</a></div> --}}
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-4 col-sm-4">
                              <div class="box_main">
                                 <h4 class="shirt_text">Eiger Eliptic Solaris</h4>
                                 <p class="price_text">Harga  <span style="color: #262626;">Rp.6.799.300</span></p>
                                 <div class="tshirt_img"><img src="https://eigeradventure.com/media/catalog/product/cache/4f33418a30da1d50c37d8b95a2cfab0e/9/1/910005207001-Z-ELIPTIC-SOLARIS-55L-1A.01.jpg"></div>
                                 <div class="btn_main">
                                    <div class="buy_bt"><a href="#">Buy Now</a></div>
                                    {{-- <div class="buy_bt"><a href="{{url('buy-produk')}}">Buy Now</a></div> --}}
                                    <div class="seemore_bt"><a href="#">See More</a></div>
                                    {{-- <div class="seemore_bt"><a href="{{ url('detail-produk') }}">See More</a></div> --}}
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <a class="carousel-control-prev" href="#main_slider" role="button" data-slide="prev">
            <i class="fa fa-angle-left"></i>
            </a>
            <a class="carousel-control-next" href="#main_slider" role="button" data-slide="next">
            <i class="fa fa-angle-right"></i>
            </a>
         </div>
      </div>
      <!-- fashion section end -->
      
      @include('template-website.footer')

   </body>
</html>